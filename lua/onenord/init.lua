-- Colorscheme name:    nord.nvim
-- Description:         Port of articicestudio's nord theme for neovim

local util = require('onenord.util')

-- Load the theme
local set = function ()
  util.load()
end

return { set = set }
